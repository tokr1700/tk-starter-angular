var app = require ('../app');

app.controller('App', function()
{
	var vm = this;


	// locals



	// public

	vm.title = 'tk-starter-angular';

	vm.colors = [
		{name: 'white', value:'#ffffff'},
		{name: 'purple', value:'#aa00aa'},
		{name: 'orange', value:'#ff9900'}
	];

	vm.currentColor = vm.colors[0];

	vm.bg_style = {'background-color': vm.currentColor.value};

	vm.update_bg_style = function ()
	{
		vm.bg_style = {'background-color': vm.currentColor.value}
	}
})
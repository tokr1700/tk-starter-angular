var app = require ('../app');

app.controller('Money', function()
{
	var vm = this;


	// locals

	var counter = 0,
		CURRENCY = 'EUR',
		MULTIPLIER = 2;

	function stringifyMoney (_amount, _currency)
	{
		return (Math.round(_amount * 100)/100).toFixed(2) + ' ' + _currency;
	}


	// public

	vm.increase = function ()
	{
		counter++;
	}

	vm.getMoney = function ()
	{
		return stringifyMoney (counter*MULTIPLIER, CURRENCY);
	}
})
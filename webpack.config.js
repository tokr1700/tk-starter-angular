var BrowserSyncPlugin = require('browser-sync-webpack-plugin');

module.exports =
{
	entry: './src/js/main.js',
	output: {
		filename: 'bundle.js',
		path: './build/'
	},
	plugins: [
		new BrowserSyncPlugin({
			// browse to http://localhost:3000/ during development,
			host: 'localhost',
			port: 3000,
			server: {
				baseDir: './'
			},
			startPath: "/src/index.html",
			files: ['./src/**/*.html', './src/**/*.css', './src/**/*.js']
		})
	]
};